import pytest
import logging
import time
import os
from _datetime import datetime
from selenium import webdriver
from pages.home_page import HomePage
from test.logg import get_file_log_handler, get_logging_dir, get_stdout_log_handler



def pytest_addoption(parser):
    parser.addoption("--chrome", action="store")
    parser.addoption("--url", action="store")


@pytest.fixture()
def chrome_version(request):
    return request.config.getoption("--chrome")


# @pytest.fixture()
# def url_selenium(request):
#     return request.config.getoption("--url")


@pytest.fixture()
def cap(chrome_version):
    options = webdriver.ChromeOptions()
    caps= options.to_capabilities()
    caps["enableVNC"] = True
    caps["browserName"] = "chrome"
    caps["version"] = "latest"
    return dict(caps)


@pytest.fixture(scope="function")
def home_page(request):
    driver = webdriver.Chrome()
    # cap = {
    #     "browserName": "chrome",
    #     "version": "latest"
    # }
    # driver = webdriver.Remote(command_executor="http://localhost:4444/wd/hub",
    #                           desired_capabilities=cap)
    base_page = HomePage(driver)

    def driver_teardown():
        time.sleep(5)
        driver.close()

    request.addfinalizer(driver_teardown)
    yield base_page


LOG_LEVEL = getattr(logging, os.getenv('LOG_LEVEL', 'INFO'))


def setup_logger():
    file_path = os.path.join(get_logging_dir(), 'tests.log')
    file_handler = get_file_log_handler(file_path, LOG_LEVEL)
    stdout_handler = get_stdout_log_handler(LOG_LEVEL)
    handlers = [file_handler, stdout_handler]
    logging.basicConfig(handlers=handlers, level=LOG_LEVEL)


setup_logger()
logger = logging.getLogger(__name__)


def pytest_runtest_call(__multicall__):
    try:
        __multicall__.execute()
    except KeyboardInterrupt:
        raise


def pytest_runtest_setup(item):
    logger.info(f'\n\n{"<"*10} {item.name} {">"*10}')
