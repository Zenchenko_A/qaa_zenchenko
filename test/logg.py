import logging
import os
import sys
from typing import List

formatter = logging.Formatter('%(asctime)s - %(name)-10s - %(levelname)s - %(message)s')


def get_logging_dir() -> str:
    log_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), "../"))
    if not os.path.exists(log_dir):
        log_dir = "/var/log/"
    return log_dir


def get_file_log_handler(file_path: str, level: str) -> logging.Handler:
    file_handler = logging.FileHandler(file_path, mode='w')
    file_handler.setFormatter(formatter)
    file_handler.setLevel(level)
    return file_handler


def get_stdout_log_handler(level: str) -> logging.Handler:
    stream_handler = logging.StreamHandler(sys.stderr)
    stream_handler.setFormatter(formatter)
    stream_handler.setLevel(level)
    return stream_handler


class BufferingHandler(logging.Handler):
    """Log handler with in memory storage"""

    def __init__(self) -> None:
        logging.Handler.__init__(self)
        self.buffer = []

    def emit(self, record: logging.LogRecord) -> None:
        self.buffer.append(record.getMessage())

    def flush(self) -> List[str]:
        """
        get stored messages
        :return: List[str]
        """
        self.acquire()
        result, self.buffer = self.buffer, []
        self.release()
        return result


def get_buffering_log_handler(level: str) -> logging.Handler:
    buff_handler = BufferingHandler()
    buff_handler.setFormatter(formatter)
    buff_handler.setLevel(level)
    return buff_handler
