from pages.base_page import BasePage
from pages.lt_page import LtPage


class CompPage(BasePage):
    url = 'https://shop.by/kompyutery/'
    lt_button = '/html/body/div[2]/div[1]/div/div[2]/div[3]/div[2]/div[1]/div/div/div[1]/div[1]/div/div/a'

    def __init__(self, driver):
        self.driver = driver

    def click_lt(self):
        self.click_button('lt')
        return LtPage(self.driver)