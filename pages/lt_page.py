from pages.base_page import BasePage


class LtPage(BasePage):
    url = 'https://shop.by/noutbuki/'
    lenovo_checkbox_button = '//*[@href="/noutbuki/lenovo/"]/label'
    dell_checkbox_button = '//*[@href="/noutbuki/dell/"]/label'
    hp_checkbox_button = '//*[@href="/noutbuki/hp/"]/label'

    find_button = '/html/body/div[2]/div[1]/div/div[2]/div[4]/div[1]/div/div[1]/div[2]/form/div/div[2]/div/span'
    see_more_brands_button = '//*[@id="Attr_prof_1000"]/div/div[2]/span[1]'
    see_more_sizes_button = '//*[@id="Attr_prof_5828"]/div/div[2]'


    twelve_xpath = '//*[@href="/noutbuki/12_dyuymov/"]/input'
    twelve_and_half_xpath = '//*[@id="ContAttr_prof_5828"]/div[22]/span/label'

    min_price_form = '//*[@id="minnum_45"]'
    max_price_form = '//*[@id="maxnum_45"]'

    first_element_xpath = '/html/body/div[2]/div[1]/div/div[2]/div[4]/div[2]/div[5]/div[1]/div[1]/div[1]'

    sort_burg_button = '//*[@id="sel5SS_chzn"]/span[1]/span'
    from_cheap_button = '//*[@id="selZ1K_chzn_o_1"]'
    to_cheap_button = '//*[@id="selZ1K_chzn_o_1"]'


    def __init__(self, driver):
        self.driver = driver
        self.open()
