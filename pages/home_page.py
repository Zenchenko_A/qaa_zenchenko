import logging
from pages.base_page import BasePage
from pages.computers import CompPage

logger = logging.getLogger(__name__)


class HomePage(BasePage):
    url = 'https://shop.by/'
    comp_button = '//*[@id="cataloglink-944"]/a/span[2]/span'

    def __init__(self, driver):
        self.driver = driver
        self.open()

    def click_comps(self):
        self.click_button('comp')
        return CompPage(self.driver)
