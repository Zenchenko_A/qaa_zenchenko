import time
import logging

from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.action_chains import ActionChains

logger = logging.getLogger(__name__)


class BasePage:
    url = None

    def __init__(self, driver):
        self.driver = driver

    def assert_visible(self, xpath):
        assert self.driver.find_element_by_xpath(xpath).is_displayed()

    def open(self):
        url = self.url
        logger.info(f'{"opening url: "} {self.url}')
        self.driver.get(url)
        return self

    def close(self):
        self.driver.close()

    def wait_until_clickable(self, xpath):
        element = WebDriverWait(self.driver, 10).until(
            ec.element_to_be_clickable((By.XPATH, xpath))
            )
        self.assert_visible(xpath)
        return element

    def full_screen(self):
        self.driver.fullscreen_window()
        return self

    def set_window_size(self, width, height):
        logger.info(f'{"set window size: width- "} {width} {", height-"} {height}')
        self.driver.set_window_size(width, height)

    def quit_driver(self):
        self.driver.quit()

    def click_button(self, name):
        xpath = getattr(self.__class__, f'{name}_button')
        actions = ActionChains(self.driver)
        actions.move_to_element(self.driver.find_element_by_xpath(xpath)).perform()
        logger.info(f'{"click button: "} {name} {" by xpath: "} {xpath}')
        try:
            element = self.wait_until_clickable(xpath)
            time.sleep(3)
            element.click()
        except Exception as e:
            logger.error(f'Can not click button: {name} by xpath: {xpath}')
            logger.exception(str(e))
            raise e
        return self

    def move_to_xpath(self, xpath):
        element = self.driver.find_element_by_xpath(xpath)
        actions = ActionChains(self.driver)
        actions.move_to_element(element).perform()
        return self

    def click_if_posible(self, name):
        xpath = getattr(self.__class__, f'{name}_xpath')
        self.driver.execute_script("arguments[0].scrollIntoView();", self.driver.find_element_by_xpath(xpath))
        if ec.element_to_be_clickable((By.XPATH, xpath)):
            time.sleep(0.5)
            self.driver.find_element_by_xpath(xpath).click()

    def send_form(self, name, key):
        xpath = getattr(self.__class__, f'{name}_form')
        logger.info(f'{"send key: "} {key} {" to form: "} {name} {" with xpath: "} {xpath}')
        try:
            self.driver.find_element_by_xpath(xpath).send_keys(key)
        except Exception as e:
            logger.error(f'Can not send key: {key} to form: {name}')
            logger.exception(str(e))
            raise e
        time.sleep(3)
        return self

    def scroll_down(self, height):
        self.driver.execute_script(f'window.scrollTo(0, {height})')
        return self

    def save_element(self, xpath):
        element = self.driver.find_element_by_xpath(xpath)
        return self, element

    def counter(self, xpath):
        count = len(self.driver.find_elements_by_xpath(xpath))
        logger.info(f'Page has {count} elements')



