FROM python:3.6-alpine
RUN pip install --upgrade pip
RUN pip install pytest && pip install selenium
COPY pages /pages
COPY test /test
COPY pytest.ini /pytest.ini